import allure
import pytest
import yaml



def get_datas():
    with open("testdatas.yml") as f:
        datas = yaml.safe_load(f)
        return datas

class TestCal:
    @allure.story("int型加法计算")
    @pytest.mark.parametrize('a,b,expect', get_datas()["add_int"]["datas"])
    def test_add_int(self, a, b, calculate, expect):
        """
        def calculate():
             print("开始计算")
             cal = Calculator()
             yield cal
             print("结束计算")
        test_add_int形参的calculate为Calculator对象，相当于如下next(p)返回值
            p=calculate()
            next(p)
        """

        assert expect == calculate.add(a, b)

    @allure.story("float型加法计算")
    @pytest.mark.parametrize('a,b,expect', get_datas()["add_float"]["datas"])
    def test_add_float(self, a, b, calculate, expect):
        assert expect == round(calculate.add(a, b), 2)

    @allure.story("负数的加法计算")
    @pytest.mark.parametrize('a,b,expect', get_datas()["add_minus"]["datas"])
    def test_add_minus(self, a, b, calculate, expect):
        assert expect == round(calculate.add(a, b), 2)

    """
    @pytest.mark.parametrize('a,b,expect', [
        [-1, -1, -2], [-0.3, -0.6, -0.9]
    ])
    def test_div(self):
        cal = Calculator()
       
                try:
            cal.div(1, 0)
        except ZeroDivisionError:
            print("除数为0")
        
        with pytest.raises(ZeroDivisionError):
            cal.div(1, 0)
    """

    @allure.story("int型除法计算")
    @pytest.mark.parametrize('a,b,expect', get_datas()["div_int"]["datas"])
    def test_div_int(self, a, b, calculate, expect):
        if b == 0:
            print("除数为0，无法计算")
        else:
            assert expect == calculate.div(a, b)

    @allure.story("float型除法计算")
    @pytest.mark.parametrize('a,b,expect', get_datas()["div_float"]["datas"])
    def test_div_float(self, a, b, calculate, expect):
        if b == 0:
            print("除数为0，无法计算")
        else:
            assert expect == round(calculate.div(a, b), 2)

    @allure.story("负数除法计算")
    @pytest.mark.parametrize('a,b,expect', get_datas()["div_minus"]["datas"])
    def test_div_minus(self, a, b, calculate, expect):
        if b == 0:
            print("除数为0，无法计算")
        else:
            assert expect == round(calculate.div(a, b), 2)





