# yield 关键字，一般用于生成器，激活了teardown操作

def provider():
    for i in range(0, 10):
        print("start")
        yield i  # 相当于return操作，返回数据，并且记录上一次的执行位置，下一次的时候继续执行
        print("end")


p = provider()
print(next(p))  # start->print(0)
print(next(p))  # end ->start->print(1)
print(next(p))  # end ->start->print(2)
