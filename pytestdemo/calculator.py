import allure



@allure.feature("计算器功能测试")
class Calculator:
    @allure.story("加法计算")
    def add(self, a, b):
        return a + b

    @allure.story("减法计算")
    def div(self, a, b):
        return a / b
