import pytest


class TestCart:
    # @pytest.fixture(autouse=True)  # 加上autouse=True，会让每条测试用例在执行的时候都调用login
    @pytest.fixture()
    def login(self):
        # yield前面相当于setup
        print("这里是登录操作")
        token = "agcbdhrhhjfduer"
        yield token    # 相当于return操作
        # yield是关键字，一般用于生成器，激活了teardown操作
        print("实现登出操作")
        # yield后面相当与teardown

    def test_search(self, login):
        print("搜索")

    def test_cart(self, login):
        print("购物车")

    # @pytest.mark.usefixtures('login')   # login可放入函数的参数里面，也可以通过装饰器调用
    def test_order(self, login):
        print("下单")