import pytest

from .calculator import Calculator
import sys
print(sys.path)

@pytest.fixture(scope='class')
def calculate():
    print("开始计算")
    cal = Calculator()
    yield cal
    print("结束计算")



# p=calculate()
# calculate1=next(p)
# test_add_int(10,20,calculate1,expect)
# test_add_int(100,200,calculate1,expect)
# next(p)

