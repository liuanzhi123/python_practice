from python_study.test_selenium.page.base_page import BasePage
from python_study.test_selenium.page.contact_page import ContactPage


class AddMemberPage(BasePage):
    def goto_contact_page(self):
        pass

    def add_member(self):
        self.driver.find_element_by_css_selector(".ww_indexImg_AddMember").click()
        self.driver.find_element_by_id("username").send_keys("liuanzhi")
        self.driver.find_element_by_id("memberAdd_acctid").send_keys("123456")
        self.driver.find_element_by_id("memberAdd_phone").send_keys("12345678910")
        self.driver.find_element_by_css_selector(".qui_btn.ww_btn.js_btn_save").click()
        return ContactPage(self.driver)
