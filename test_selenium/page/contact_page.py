from time import sleep

from selenium.webdriver.common.by import By

from python_study.test_selenium.page.add_department_page import AddDepartmentPage
from python_study.test_selenium.page.base_page import BasePage


class ContactPage(BasePage):
    __add_click = (By.CSS_SELECTOR, ".member_colLeft_top_addBtn")
    __add_department = (By.CSS_SELECTOR, ".js_create_party")

    def goto_add_member_page(self):
        pass

    def get_list(self):
        ele_list = self.driver.find_element_by_css_selector()
        print(ele_list)
        name_list = []
        for i in ele_list:
            name_list.append(i.text)
        print(name_list)
        return name_list

    # 跳转添加部门页面
    def goto_add_department(self):

        self.click(self.__add_click, self.__add_department)
        # 需返回类的实例化，并且需要传入参数self.driver,因为该类继承了基类BasePage，且添加部门页面操作时需要使用self.driver
        return AddDepartmentPage(self.driver)

    def get_department_list(self):

        ele_list = self.driver.find_elements_by_css_selector(".jstree-children .jstree-node .jstree-anchor")
        print(ele_list)
        name_list = [i.text for i in ele_list]
        # for i in ele_list:
            # name_list.append(i.text)
        print(name_list)
        return name_list

    # 获取通讯录名单
    def get_address_list(self):
        pass

