from selenium.webdriver.common.by import By

from python_study.test_selenium.page.add_member_page import AddMemberPage
from python_study.test_selenium.page.base_page import BasePage
from python_study.test_selenium.page.contact_page import ContactPage


class MainPage(BasePage):
    __contact_click = (By.XPATH, "//*[@id='menu_contacts']/span")

    def goto_add_member_page(self):
        self.driver.implicitly_wait(5)
        self.driver.find_element_by_css_selector(".ww_indexImg_AddMember").click()
        return AddMemberPage(self.driver)

    def goto_contact_page(self):
        self.click(self.__contact_click)
        return ContactPage(self.driver)

    def import_address_book(self):
        pass
    