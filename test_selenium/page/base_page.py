from selenium import webdriver


class BasePage:
    def __init__(self, base_driver = None):
        """
        base_driver:传入driver实例对象
        """
        # 如果base_driver是初始值None,那么就会实例化实例化driver
        if base_driver is not None:
            self.driver = base_driver
        else:
            # 浏览器复用模式
            chrome_arg = webdriver.ChromeOptions()
            # 加入调试地址
            chrome_arg.debugger_address = "127.0.0.1:9222"
            # 实例化driver对象
            self.driver = webdriver.Chrome(options=chrome_arg)
            self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
            # 隐式等待，会在每次find操作的时候，轮询查找该元素，超时报错
            self.driver.implicitly_wait(8)

    def find(self, locator):
        return self.driver.find_element(*locator)

    def click(self, *locators):
        for locator in locators:
            self.find(locator).click()

    def send_keys(self, locator, part):
        self.find(locator).send_keys(part)


