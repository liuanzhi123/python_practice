from time import sleep

from selenium.webdriver.common.by import By

from python_study.test_selenium.page.base_page import BasePage


class AddDepartmentPage(BasePage):
    __department_name = (By.XPATH, "//*[@id='__dialog__MNDialog__']/div/div[2]/div/form/div[1]/input")
    __parent_department = (By.CSS_SELECTOR, ".js_parent_party_name")
    __department = (By.CSS_SELECTOR, ".ww_dialog .jstree-children  [id='1688852055981679_anchor']")
    __commit = (By.XPATH, "//*[@id='__dialog__MNDialog__']/div/div[3]/a[1]")

    def add_department(self):
        self.send_keys(self.__department_name, "测试部")

        self.click(self.__parent_department, self.__department, self.__commit)

        """
        self.driver.find_element_by_xpath("//*[@id='menu_contacts']/span").click()
        self.driver.find_element_by_css_selector(".member_colLeft_top_addBtn").click()
        self.driver.find_element_by_css_selector(".js_create_party").click()
        self.driver.find_element_by_xpath("//*[@id='__dialog__MNDialog__']/div/div[2]/div/form/div[1]/input").send_keys("测试部")
        self.driver.find_element_by_css_selector(".js_parent_party_name").click()
        # 单独根据id定位，无法定位到所属部门，需要先定位弹框，在定位所属部门的id        
        self.driver.find_element_by_css_selector(".ww_dialog .jstree-children  [id='1688852055981679_anchor']").click()
        self.driver.find_element_by_xpath("//*[@id='__dialog__MNDialog__']/div/div[3]/a[1]").click()
        
        """

        from python_study.test_selenium.page.contact_page import ContactPage
        return ContactPage(self.driver)
