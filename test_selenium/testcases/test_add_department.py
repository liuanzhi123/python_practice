from python_study.test_selenium.page.main_page import MainPage


# 不要继承带有__init__方法的BasePage类，会导致pytest无法收集到cases
class TestAddDepartment():
    # 通过浏览器复用模式运行脚本，需要先调试浏览器，调试正常后，在调试中会打开浏览器，
    # 在该浏览器中重新打开一个新页面，登录企业微信，之后在跑脚本，调试之后一定不要关闭浏览器！！！
    def test_add_department(self):
        self.main = MainPage()
        result1 = self.main.goto_contact_page().goto_add_department().add_department().get_department_list()
        assert "测试部" in result1