from python_study.test_selenium.page.base_page import BasePage
from python_study.test_selenium.page.main_page import MainPage


class TestAddMember(BasePage):
    def test_add_member(self):
        self.main = MainPage()
        result = self.main.goto_add_member_page().add_member().get_list()
        assert "皮城女警" in result



