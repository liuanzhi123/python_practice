from Python_practice.HERO import Hero
from Python_practice.timo import Timo
from Python_practice.hero_factory import HeroFactory


#timo=HeroFactory.create_hero("timo")
timo = HeroFactory.create_hero('timo')
police = HeroFactory.create_hero('police')
won = police.fight(timo)
if won is not None:
    won.speak_lines()
# police.speak_lines()
# timo.speak_lines()