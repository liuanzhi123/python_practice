from Python_practice.EZ import EZ
from Python_practice.JINX import Jinx
from Python_practice.police import Police
from Python_practice.timo import Timo


class HeroFactory:
    """
    简单工厂模式专门定义一个类来负责创建其他类型的英雄的实例
    """
    # 1、职责清晰
    # 2、提供了一个入口，比如添加了新的英雄，其他研发调用代码的过程中，可以以factory为主，不需要一个文件一个文件去读写内容
    @classmethod
    def create_hero(cls , name):
        if name == "timo":
            return Timo()
        elif name == "police":
            return Police()
        else:
            raise Exception("此英雄不在英雄工厂中")