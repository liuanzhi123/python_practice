class Hero:
    hp = 0
    power = 0
    name = ""

    def fight(self, timo):
        '''
        返回胜利者对象,打平时返回None
        '''
        finnal_hp = self.hp - timo.power
        enemy_finnal_hp = timo.hp - self.power

        if finnal_hp > enemy_finnal_hp:
            print(f"{self.name}赢了")
            return self
        elif finnal_hp < enemy_finnal_hp:
            print("敌人赢了")
            return timo
        else:
            print("打平了")
            return None

    def speak_lines(self):
        pass